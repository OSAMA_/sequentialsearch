package com.epam.de.dsa;

public class SequentialSearch {

    public int search(int[] space, int value) {
        for(int i=0;i<space.length;i++){
            if(space[i]==value){
                return i+1;
            }
        }
        return 0;
    }
}
