package com.epam.de.dsa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SequentialSearchTest {

    SequentialSearch sequentialSearch;

    @BeforeEach
    void setUp(){
        sequentialSearch = new SequentialSearch();
    }

    @Test
    void searchTest(){
        int[] arr = {10,2,13,41,15};
        assertEquals(1,sequentialSearch.search(arr,10));
        assertEquals(0,sequentialSearch.search(arr,22));
    }
}